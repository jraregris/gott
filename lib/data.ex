defmodule Gott.Data do
  @gott_dir ".gott"
  @objects_dir "#{@gott_dir}/objects"

  def gott_dir(), do: @gott_dir

  def init() do
    File.mkdir!(@gott_dir)
    File.mkdir!(@objects_dir)
  end

  def hash_object(content, type \\ :blob) do
    obj = to_string(type) <> <<0>> <> content

    oid =
      :crypto.hash(:sha, obj)
      |> Base.encode16(case: :lower)

    File.write!("#{@objects_dir}/#{oid}", obj)
    oid
  end

  def get_object(oid, expected_type \\ :blob) do
    obj = File.read!("#{@objects_dir}/#{oid}")
    [actual_type, content] = String.split(obj, <<0>>, parts: 2)

    if expected_type != nil && to_string(expected_type) != actual_type do
      IO.puts("Expected #{expected_type}, got #{actual_type}")
      exit(1)
    end

    content
  end
end
