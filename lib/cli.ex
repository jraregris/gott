defmodule Gott.CLI do
  alias Gott.Data
  alias Gott.Base

  def main(argv \\ []) do
    case argv do
      ["init" | _args] -> init()
      ["hash-object" | args] -> hash_object(args)
      ["cat-file" | args] -> cat_file(args)
      ["write-tree" | args] -> write_tree(args)
      _args -> IO.puts("wrong")
    end
  end

  def init() do
    Data.init()
    IO.puts("Initializd empty gott repository in #{File.cwd!()}/#{Data.gott_dir()}")
  end

  def hash_object(argv) do
    {_parsed, args, _invalid} = OptionParser.parse(argv, strict: [])

    case args do
      [filename] ->
        hash =
          File.read!(filename)
          |> Data.hash_object()

        IO.puts(hash)

      _ ->
        IO.puts("Bad argument, supply exactly one file name")
        exit(:bad_argument)
    end
  end

  def cat_file(argv) do
    {_parsed, args, _invalid} = OptionParser.parse(argv, strict: [])

    case args do
      [oid] -> IO.puts(Data.get_object(oid))
    end
  end

  def write_tree(_args) do
    Base.write_tree()
    |> IO.puts()
  end
end
