defmodule Gott.Base do
  alias Gott.Data

  def write_tree(directory \\ ".") do
    entries =
      for entry <- File.ls!(directory), !is_ignored(entry) do
        full = "#{directory}/#{entry}"

        {oid, type} =
          case File.stat!(full).type do
            :regular ->
              oid =
                File.read!(full)
                |> Data.hash_object()

              {oid, :blob}

            :directory ->
              oid = write_tree(full)
              {oid, :tree}
          end

        {entry, oid, type}
      end

    tree =
      for {name, oid, type} <- entries |> Enum.sort() do
        "#{type} #{oid} #{name}\n"
      end
      |> Enum.join()

    Data.hash_object(tree, :tree)
  end

  def is_ignored(path) do
    split_path = String.split(path, "/")

    ".gott" in split_path ||
      ".git" in split_path ||
      ".elixir_ls" in split_path ||
      "_build" in split_path
  end
end
