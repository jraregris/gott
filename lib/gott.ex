defmodule Gott do
  @moduledoc """
  Documentation for `Gott`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Gott.hello()
      :world

  """
  def hello do
    :world
  end
end
